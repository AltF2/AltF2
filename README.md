<h1>Thanks for visiting my profile 👋</h1>

Hoi! I'm Matthew, I am an passionate FOSS developer from 🇳🇱. I am currently working personal hobby projects. I love to experiment with new techonolgies and try to come up with solutions to the weirdest problems. I'm always open for a challenge!
<br/>
<details>
      <summary> Languages </summary>
Here are the languages I know/am learning:
<br>
- C
<br>
- Python
<br>
- Rust
<br>
- JavaScript/TypeScript
<br>
- A bit of go
<br>
- A bit of .NET
<br>
- A bit of Java
<br> 
- Basics of x86 and RISC-V assembly
<br />
<br />
</details>
<details>
      <summary> Statistics </summary>
<br/>
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=AltF02&show_icons=true&include_all_commits=true&theme=dracula" alt="AltF02's github stats" />
      <br />
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=AltF02&layout=compact&theme=dracula" />
  <br />
  <!--START_SECTION:waka-->
**🐱 My GitHub Data** 

> 🏆 224 Contributions in the Year 2022
 > 
> 📦 39.4 kB Used in GitHub's Storage 
 > 
> 💼 Opted to Hire
 > 
> 📜 59 Public Repositories 
 > 
> 🔑 20 Private Repositories  
 > 
**I Mostly Code in Rust** 

```text
Rust                     25 repos            █████████░░░░░░░░░░░░░░░░   36.23% 
Python                   19 repos            ███████░░░░░░░░░░░░░░░░░░   27.54% 
JavaScript               8 repos             ███░░░░░░░░░░░░░░░░░░░░░░   11.59% 
TypeScript               4 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   5.8% 
Vue                      3 repos             █░░░░░░░░░░░░░░░░░░░░░░░░   4.35%

```



 Last Updated on 13/07/2022 20:52:22 UTC
<!--END_SECTION:waka-->
  </details>
  <details>
      <summary> Popular projects</summary>
  <a href="https://github.com/AltF02/x11-rs">
  <img align="center" src="https://github-readme-stats.vercel.app/api/pin/?username=AltF02&repo=X11-rs&theme=dracula" /> 
  </a>
  </details>
<details>
      <summary> Contact </summary>
<br/>
My contact details are available on <a href="https://altf2.dev">my site</a>
<br/>
</details>
  <p align="center"> 
  Visitor count<br>
  <img src="https://profile-counter.glitch.me/AltF02/count.svg" />
</p>


